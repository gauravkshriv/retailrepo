import { Component, OnInit ,ViewChild, ElementRef, NgZone, ChangeDetectorRef} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import * as $ from 'jquery';
import { HttpClient, HttpHeaders } from '@angular/common/http';
declare var $ :any; 
import Swal from 'sweetalert2';
import { MapsAPILoader } from '@agm/core';
import { ApiService,PincodeService,UtilService } from '../services';
import {Encryptor} from '../services/encryption.service';
@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {
  close_navi:boolean;
   uuid:any;
   zoom: number=1
   latitude: number;
   longitude: number;  
   jwttoken:any;
   sessiontoken:any;
   username:any;
   fullname:any;
   profiledata:any;
   serverData:any;
   occupation:any;
   stateprop:any;
   state:any;
   kycstatus:any;
   showkyc:any;
   show: boolean = localStorage.getItem("userLoginStatus")  == "true" ? true : false;
   showprofile: boolean = localStorage.getItem("userLoginProfile") == "true" ? true : false;
  constructor( private encryptor: Encryptor, private utilservice:UtilService, private dataservice:PincodeService, private apiservice:ApiService, private router: Router,private http: HttpClient) {
    this.uuid = localStorage.getItem("uuid");
    this.jwttoken = localStorage.getItem("jwttoken");
    this.sessiontoken = localStorage.getItem("sessiontoken");
    this.fullname = localStorage.getItem("fullname");
    this.username = localStorage.getItem("username");
    this.occupation = localStorage.getItem("occupation");

    this.kycstatus = localStorage.getItem("kycstatus");
    
    this.state = localStorage.getItem("state");  

    console.log("landowner",this.occupation);
   }
   

   


  ngOnInit() {

    this.ValidateLogin();
    this.GetUSer();

    this.router.events.subscribe(() => {
      this.closeNav();
    });

    $(".routlet_sec").click(function() {
      let wid=$('#mySidenav').width();
      if(wid==280){
          $('#mySidenav').width(0);
       }
       });
       
    if(localStorage.getItem("Userdetails"))
    {
      if(localStorage.getItem("fullname") === 'null'){
        swal({
          title:"Thank You!!!",
          text: "Please complete information Detail on our RME Portal.",
          icon: "success",
          buttons: {
            cancel: {
              text: "Cancel",
              value: false,
              visible: true,
              className: "",
              closeModal: true,
            },
            confirm: {
              text: "Complete Information",
              value: true,
              visible: true,
              className: "",
              closeModal: true,
            }
            
          },
          // button: false,
          //closeOnClickOutside: false
        }) .then(data => {
          console.log("data Authentication  error ",data)
          if(data)
          this.SendLoadData(); 
          else{
              this.router.navigate(['/dashboard'],{replaceUrl:true});
            }
        });
      }
      else if(localStorage.getItem("kycstatus") === 'PENDING' || localStorage.getItem("kycstatus") === 'ERROR'){
        console.log("no data to auth");
        swal({
          title:"Thank You!!!",
          text: "Please complete KYC Detail on our RME Portal.",
          icon: "success",
          buttons: {
            cancel: {
              text: "Cancel",
              value: false,
              visible: true,
              className: "",
              closeModal: true,
            },
            confirm: {
              text: "Complete KYC",
              value: true,
              visible: true,
              className: "",
              closeModal: true,
            }
            
          },
          // button: false,
          //closeOnClickOutside: false
        }) .then(data => {
          console.log("data Authentication  error ",data)
          if(data)
          this.SendDataKyc(); 
          else{
              this.router.navigate(['/dashboard'],{replaceUrl:true});
            }
        });
      }
    }
   

 


    // if(this.kycstatus == null)
    // {
    //  this.SendDataKyc();
    // }

 

      this.dataservice.getFacilities().subscribe((data:any)=>
    {
      this.stateprop = data[34].PROPERTYINSTATE;
      console.log("property data",this.stateprop);
      // this.step1Section();

    });
      
  }


   

  openNav() {
    $('#mySidenav').css('width','280')
    console.log("open nav");
    }

  closeNav() {
    $('#mySidenav').css('width','0');
    // console.log("close nav")
  }

  SendLoadData() {
    var dataencript = [
      { jwttoken: localStorage.getItem("jwttoken") },
      { uuid: localStorage.getItem("uuid") },
      { sessiontoken: localStorage.getItem("sessiontoken") }
    ]
    let enc = this.encryptor;
    var ciphertext = (enc.encrypt(JSON.stringify(dataencript)));
    window.location.href = 'https://account.rmehub.in/personalinformation?referral=rmeretail&&redirect=https://retail.rmehub.in/login&&_ct=' + btoa(ciphertext);
    // console.log("=======================>ciphertext", ciphertext);
  }

  SendDataKyc() {
    var dataencript = [
      { jwttoken: localStorage.getItem("jwttoken") },
      { uuid: localStorage.getItem("uuid") },
      { sessiontoken: localStorage.getItem("sessiontoken") }
    ]
    let enc = this.encryptor;
    var ciphertext = (enc.encrypt(JSON.stringify(dataencript)));
    window.location.href = 'https://account.rmehub.in/kycdetails?referral=rmeretail&&redirect=https://retail.rmehub.in/login&&_ct=' + btoa(ciphertext);
    // console.log("=======================>ciphertext", ciphertext);
  }


  // validcondition()
  // {
  //   if(localStorage.getItem("Userdetails") == null &&
  //   localStorage.getItem("jwttoken") == null &&
  //   localStorage.getItem("sessiontoken") == null)
  //   {
  //     swal({
  //       title:"Authentication Error!!!",
  //       text: "Kindly Login to view this page",
  //       icon: "error",
  //       buttons: {
  //         cancel: {
  //           text: "Cancel",
  //           value: false,
  //           visible: true,
  //           className: "",
  //           closeModal: true,
  //         },
  //         confirm: {
  //           text: "Login Now",
  //           value: true,
  //           visible: true,
  //           className: "",
  //           closeModal: true,
  //         }
          
  //       },
  //       // button: false,
  //       //closeOnClickOutside: false
  //     }) .then(data => {
  //       console.log("data Authentication  error ",data)
  //       if(data)
  //       window.location.href = 'http://accounttest.s3-website.us-east-2.amazonaws.com/?referral=rmeretail&returnUrl='+this.router.url;
  //       // window.location.href="http://accounttest.s3-website.us-east-2.amazonaws.com/?referral=rmeretail,";
  //         else{
  //           this.router.navigate(['/dashboard'],{replaceUrl:true});
  //         }
  //       });
        
  //       return 1;
  //   }
  //  else if(localStorage.getItem("occupation") !== "LAND_OWNER" ||  localStorage.getItem("occupation") !== "PROJECT_MENTOR")
  //   {
  //     swal({
  //       title: "Not Allowed.",
  //       text: "Allowed For just Land Owner and Project Mentor !!",
  //       icon: "info",
  //       closeOnEsc: false,
  //       closeOnClickOutside: false,
  //       })

  //       return 2;
  //   }
  //   else{
      
      
  //     return 3;
  //   }
  
  // }

  // sellproperty()
  // {
  //   if(this.validcondition() == 3)this.router.navigate(['/sell-property']);
  // }
  
  // sellrawland()
  // {
  //   if(this.validcondition() == 3)this.router.navigate(['/sell-rawland']);
  // }
  

  // rawsellvalid()
  // {
  //   if(this.validcondition() == 3)  this.router.navigate(['/onwer-profile']);    
  // }


  Logout()
  {
    let body = {
      'uuid': this.uuid,
      "sessionToken": this.sessiontoken
    };
    console.log("this is------>", body)

    var self = this;
    this.apiservice.LogOutPortal(body)
      .subscribe(
        (data) => {
          this.utilservice.Logout(data);
        });
  }
  statepropfilter(e,state)
  {
    console.log("e",e.target.value,state.name);
  }



  loginRoute()
  {
      window.location.href='https://account.rmehub.in/?referral=rmeretail&returnUrl='+this.router.url;
   }
  
  GetUSer() {
    if (this.uuid==null || this.sessiontoken==null || this.jwttoken==null)
    {
      localStorage.clear();
      console.log("not working");
    }
    else{
      let body = {
        'uuid': this.uuid,
        "sessionToken": this.sessiontoken
      };
        this.apiservice.getuserdata(body)
          .subscribe(
            (data) => {
              this.profiledata = data;
              console.log("=============================>", this.profiledata);
              localStorage.setItem('email',this.profiledata.email);
              if (this.profiledata.firstname == null) {
                localStorage.setItem("userLoginProfile", "true");
                console.log("check red bar issue");
                this.showprofile = true;
                console.log('profile',this.showprofile);
                
              }
               else if (this.profiledata._kst == "PENDING")  {
                //  localStorage.setItem("kycstatus", "true");
                 this.showkyc = true;
               }
              
              
            }, //For Success Response
            (err) => {
              console.log("got error", err)
              //  self.serverDataLogin=err;
            } //For Error Response
          );
      }
    };
  


  
  ValidateLogin()
	{
		const httpOptions = {
			headers: new HttpHeaders({
				 'Content-Type':  'application/json',
				 'Authorization': 'Token ' + this.jwttoken})
			  };
		 	let body = {
			 'uuid': this.uuid,
			 'sessionToken':this.sessiontoken,
      };
    if (this.uuid==null || this.sessiontoken==null || this.jwttoken==null)
    {
      console.log("not working");
    }
    else{
			// console.log("this is------>",body)
		 	// var self=this;
		 this.http.post('https://api.rmehub.in/api/user/getSession', body, httpOptions)
		 	.subscribe(
 	    (data) => {
					 console.log("successful verify", data);
					 this.serverData=data;
          this.utilservice.validationException(data);		
				}, //For Success Response
			 	    (err) => {
						 console.log("got error",err)
		 	    } //For Error Response
         );
        }
	}

}
                                                                     