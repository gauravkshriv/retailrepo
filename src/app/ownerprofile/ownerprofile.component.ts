import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormArray, Validators, FormGroup } from "@angular/forms";
import { HttpClient, HttpHeaders, } from '@angular/common/http';
import {ApiService} from '../services/api.service';
import { Options,LabelType  } from 'ng5-slider';
import {EnumdataService} from '../services/enumdata.service';
import { ViewChild, ElementRef, NgZone } from '@angular/core';
import { interval } from 'rxjs';
import { Observable, Subscriber } from 'rxjs';
import { tap, map, filter } from 'rxjs/operators';
@Component({
  selector: 'app-ownerprofile',
  templateUrl: './ownerprofile.component.html',
  styleUrls: ['./ownerprofile.component.scss']
})
export class OwnerprofileComponent implements OnInit {
  isLoading=false;
  uuid:any; 
   searchControl:any;
  minValue:number;
  maxValue:number;
  jwttoken:any;
  ownerproperties:any;
  constructor(private enumservice:EnumdataService, private http: HttpClient , private apiservice:ApiService, private ngZone: NgZone) { 
    this.uuid = localStorage.getItem("uuid");
    this.jwttoken = localStorage.getItem("jwttoken");

    const  o = interval(1000).pipe(
      map(value => value * value),
      filter(value => value % 2 === 0)
    );

    o.subscribe(
      // (value)=> console.log(value)
    );
  }

  options: Options = {
    floor: 0,
    showTicks: false,
    ceil: 50000000,
    step: 500000,
    translate: (value: number, label: LabelType): string => {
      switch (label) {
        case LabelType.Low:
          return '<b>Min price:</b> ₹' + value;
        case LabelType.High:
          return '<b>Max price:</b> ₹' + value;
        default:
          return '₹' + value;
      }
    }
  };
  ngOnInit() {
    this.getStateData();
    this.getOwnerPropertyty(this.uuid);
    this.minValue = 10000;
    this.maxValue = 50000000;
  }

  
  togglestate(e)
  {
    console.log("state",e.target.value);
  }

  statedata:any;
  getStateData()
  {
    this.http.get('../assets/state.json')
    .subscribe((data:any)=>
    {
      this.statedata = data;
      console.log("data",this.statedata);
    },
    (err) => {
      console.log("got error", err)
      //  self.serverDataLogin=err;
    } //For Error Response)
  
    )
  };


  getOwnerPropertyty(uuid)
  {
    this.isLoading = true;
  this.apiservice.getOwnerProperties(uuid)
    .subscribe((data:any)=>
      {
        console.log("data",data);
        this.ownerproperties = data.extraData.Property_Listing;
        this.isLoading = false;

        this.ownerproperties.forEach((v,i) => {
          this.ownerproperties[i].furnishing = this.enumservice.kytname.get(v.furnishing)
        });
        console.log("single properties",this.ownerproperties.properties);
      },
      (err) => {
        console.log("got error", err)
        //  self.serverDataLogin=err;
      } //For Error Response)
    
      )
    };  


}
