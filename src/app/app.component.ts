import { Component,OnInit } from '@angular/core';
import {UtilService} from './services/util.service';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  title = 'rme-realbeatswap';
  constructor(private utilservice:UtilService) {
  }
   

  ngOnInit()
  {
   
      this.utilservice.setCurrentPosition();
   
    
  }

 
}
