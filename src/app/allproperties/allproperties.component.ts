import { Component, OnInit,ChangeDetectorRef,ViewChild, ElementRef, NgZone } from '@angular/core';
import { Options,LabelType  } from 'ng5-slider';
import * as $ from 'jquery';
import {  } from '../services/pincode.service';
import { NguCarousel, NguCarouselConfig } from '@ngu/carousel';
import Swal from 'sweetalert2';
import {ActivatedRoute, Router, NavigationEnd} from '@angular/router';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import { ApiService,EnumdataService,PincodeService } from '../services';
import { FormControl } from '@angular/forms';
import { ReplaySubject } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
@Component({
  selector: 'app-allproperties',
  templateUrl: './allproperties.component.html',
  styleUrls: ['./allproperties.component.scss']
})
export class AllpropertiesComponent implements OnInit {
  minValue:number;
  maxValue:number;
  isLoading=false;
  nearlocat:any;
  kitchentype:any;
  bathroom:any;
  furnishing:any;
  searchControl:FormControl
  flooring:any;
  ceiling:any;
  recentProperties:any;
  rooms:any;
  bhk:any;
  payconstructionphase:any;
  slideNo = 0;
  withAnim = true;
  resetAnim = true;
  spinner:any;
  rawproperties:any;
  page:any;
  name:any;
  mobileNo:any;
  email:any;
  city:any;
  statevalue:any;
  propertyId:any;
  statedata:any;
  dropdownSettings = {};
  cityvalue:any;
  minbuiltup:any;
  maxbuiltup:any;
  mincarpet:any;
  maxcarpet:any;
  @ViewChild('myCarousel') myCarousel: NguCarousel<any>;
  carouselConfig: NguCarouselConfig = {
   grid: { xs: 1, sm: 1, md: 2, lg: 3, all: 0 },
     interval: {timing: 4000, initialDelay: 1000},
     loop: true,
     slide: 1,
     load: 2,
     easing: 'ease',
     animation: 'lazy',
     touch: true,
     velocity: 0.2,
     point: {
       visible: true,
       hideOnSingleSlide: true
     }
  }

  carouselItems =[];
  carouselItemsView = [];

  @ViewChild("search") searchElementRef: ElementRef;

  constructor(private toastr: ToastrService, private enumservice:EnumdataService,private http: HttpClient,private router: Router,private route: ActivatedRoute, private cdr: ChangeDetectorRef,  private dataservice:PincodeService,private apiservice:ApiService) { 
    toastr.toastrConfig.preventDuplicates=true;

  }

  options: Options = {
    floor: 0,
    showTicks: false,
    ceil: 50000000,
    step: 500000,
    translate: (value: number, label: LabelType): string => {
      switch (label) {
        case LabelType.Low:  
          // return '<b>Min:</b> ₹' + value; 
         
        case LabelType.High: 
          // return '<b>Max:</b> ₹' + value;
        default:
          return '₹' + value;
          
      }
    }
    
  };


 
          

  ngAfterViewInit() {
    this.cdr.detectChanges();
  }
 
  reset() {
    this.myCarousel.reset(!this.resetAnim);
  }
 
  moveTo(slide) {
    this.myCarousel.moveTo(slide, !this.withAnim);
  }

  ngOnInit() {

    this.dropdownSettings = {
      singleSelection: true,
      idField: 'code',
      textField: 'name',
      selectAllText: 'Select All',
      unSelectAllText: 'UnSelect All',
      itemsShowLimit: 20,
      allowSearchFilter: false
    };

    $('.dropdown-menu').click(function(e) {
      // console.log('ksjdksd');
         e.stopPropagation();
      });

    this.http.get('./assets/state.json')
    .subscribe((data:any)=>
    {
      this.statedata = data;
      console.log("data",this.statedata);
    })

    this.getRecentlyUploaded(0);
    this.getTopRawLandUploaded();

    this.minValue = 10000;
    this.maxValue = 50000000;

    this.dataservice.getFacilities().subscribe((data:any)=>
    {
      this.nearlocat = data[13].NEARLOCATION;
      this.flooring = data[6].FLOORING;
      this.ceiling = data[7].CEILING;
      this.kitchentype = data[12].KITCHENTYPE;
      this.bathroom = data[11].BATHROOMTYPE;
      this.furnishing = data[1].FURNISHING;
      this.payconstructionphase = data[27].PAYCONSTRUCTIONPHASE;
      this.rooms = data[10].ROOMS;
      this.bhk = data[14].BHK;
      console.log("property Facilities data",this.nearlocat);
    });
 

    this.searchControl = new FormControl();
  }



  recentview:ReplaySubject<Array<any>>= new ReplaySubject<Array<any>>()

   
getRecentlyUploaded(page)
{
 
  this.isLoading = true;
  this.page = page
  this.apiservice.gettoRecentpproperties(this.page)
  // this.http.get('http://ec2-3-80-207-138.compute-1.amazonaws.com/filter/show/res?page=0&size=10&sort=createdAt,des')
 .subscribe((data:any)=>
     {
      console.log("recent properties",data);
      if(data.successCode === "API_SUCCESS" && data.statusCode == 200)
      {
        $(window).scrollTop({scrollTop:0}, 5000);
        // this.recentProperties = data.page.content;

        this.filterProperty = this.recentProperties = data.page.content; 

        this.isLoading = false;
        console.log("recent properties",this.recentProperties);
       // let __crousel=[];
        this.recentProperties.forEach((data,i) => {
         this.propertyId = data.propertyId
         // console.log("===>",this.recentProperties[i].pricing.assetConstructionPhase);
 
         this.recentview.next(this.recentProperties)

         this.recentProperties[i].furnishing = data.furnishing.replace('_',' ');
 
         const str2 = data.createdAt;
         const arr2 = str2.split(/ (.*)/);
         this.recentProperties[i].createdAt = new Date(arr2[0]).toDateString();


         this.recentProperties[i].pricing.assetConstructionPhase = this.enumservice.kytname.get(this.recentProperties[i].pricing.assetConstructionPhase);
         this.recentProperties[i].residentialStructuralDocument.propertyFacing = this.enumservice.kytname.get(this.recentProperties[i].residentialStructuralDocument.propertyFacing);
        //  this.recentProperties[i].prebuiltAssetType = this.enumservice.kytname.get(this.recentProperties[i].prebuiltAssetType);

     if(this.recentProperties[i].residentialStructuralDocument.flats)
    {
      this.recentProperties[i].residentialStructuralDocument.flats.bhks = this.enumservice.kytname.get(this.recentProperties[i].residentialStructuralDocument.flats.bhks);

    }

     });

      console.log("check======>?????????????", this.carouselItems);
      console.log("propertyId",this.propertyId)
      }
      else(data.exception === "NO_RECORDS_FOUND")
      {
       this.isLoading = false;
      }},
     (err) => {
       console.log("got error", err)
       //  self.serverDataLogin=err;
     } //For Error Response)
)};

    // //////////////////////////////////////////



    getTopRawLandUploaded()
    {
      this.isLoading = true;
      this.apiservice.getrawlandproperty()
    .subscribe((data:any)=>
        {
          console.log("raw properties",data);
          this.rawproperties = data.rawLandPageableResponseDto.rawLandResponse;
          this.isLoading = false;
          console.log("raw properties",this.rawproperties);
         this.rawproperties.forEach((data,i) => {
          // this.rawproperties[i].marketplace = data.marketplace.replace('_',' ');
          // this.rawproperties[i].propertyType = data.propertyType.replace('_',' ');
          this.rawproperties[i].marketplace = this.enumservice.kytname.get(this.rawproperties[i].marketplace);


           const str2 = data.createdAt;
          const arr2 = str2.split(/ (.*)/);
          this.rawproperties[i].createdAt = new Date(arr2[0]).toDateString();

       });
      //  this.carouselItems = __crousel;
       console.log("check======>?????????????", this.carouselItemsView);
        },
        (err) => {
          console.log("got error", err)
          //  self.serverDataLogin=err;
        } //For Error Response)
      
        )};


  filterArea()
  {
    $('.buy-filter').show();
  }      

  RemovefilterArea()
  {
    $('.buy-filter').hide();
  }


  alphabet(e)
  {
    var k = e.which;
    var ok = (k >= 65 && k <= 90) || // A-Z(capital letter alpahabets)
        k >= 97 && k <= 122 || // a-z(small letter alpahabets)
        k == 8 ||  // Backspaces
        k == 9 ||  //H Tab
        k == 0 ||  //H Tab
        k == 11 ||  //V Tab
        k == 32 ||  // Space
        k == 127;   //Delete
    if (!ok) {
    // prevent user to press key
        e.preventDefault();
    }
  }

  numeric(e)
  {
    var k = e.which;
    var ok = (k >= 48 && k <= 57) ||  // 0-9
        k == 8 ||  // Backspaces
        k == 9 ||  //H Tab
        k == 11 ||  //V Tab
        k == 0 ||  // Tab for Firefox
        k == 127;   //Delete
    if (!ok) {
        e.preventDefault();
    }
  }


  contactstate(e)
  {
    this.statevalue = e.target.value;
    console.log(this.statevalue);
    
  }

  // onFilterChange(event)
  // {
  //   console.log("event",event);
  
  
  // }
  minPrice = [];
  maxPrice = [];

  ApiDataResponse(e)
  {
    this.minPrice = [];
    this.maxPrice = [];
    // console.log("max",e);
  this.minPrice.push(e.value);
  // console.log("Min",this.minPrice);

  this.maxPrice.push(e.highValue);
  // console.log("Max",this.maxPrice);
  }

  statearray = [];

  togglestate(item) {
     console.log(item);
    if(!this.statearray.includes(item.name))

    this.statearray.push(item.name);
    else this.statearray.splice(this.statearray.indexOf(item.name),1)
    console.log("statearray",this.statearray);
  }


  cityarray = [];
  minbuiltarray = [];
  maxbuiltarray = [];
  mincarpetarray = [];
  maxcarpetarray = [];

  roomsarray = [];

  tooglerooms(data,e)
  {
   if(!this.roomsarray.includes(data.value))
   this.roomsarray.push(data.value);
   else{
     this.roomsarray.splice(this.roomsarray.indexOf(data.value),1);
   }
    console.log('data',this.roomsarray);
  }


  roomsbhk = [];

  tooglebhk(data,e)
  {
   if(!this.roomsbhk.includes(data.value))
   this.roomsbhk.push(data.value);
   else{
     this.roomsbhk.splice(this.roomsbhk.indexOf(data.value),1);
   }
    console.log('data',this.roomsbhk);
  }


  furnishingstatus = [];

  tooglefurnishing(data,e)
  {
   if(!this.furnishingstatus.includes(data.value))
   this.furnishingstatus.push(data.value);
   else{
     this.furnishingstatus.splice(this.furnishingstatus.indexOf(data.value),1);
   }
    console.log('data',this.furnishingstatus);
  }

  filterProperty:any;

  applyFilter(page)
  {
    $(window).scrollTop({scrollTop:0}, 5000);
    $('#pagsec .page-item').removeClass('highlight')
  console.log('page',page);
  
    $(`#pagsec .page-item:nth-child(${page})`).addClass('highlight')
    
    this.isLoading = true;
    this.cityarray = [];
    this.minbuiltarray = [];
    this.maxbuiltarray = [];
    this.mincarpetarray = [];
    this.maxcarpetarray = [];
    
    if(this.city) this.cityarray.push(this.cityvalue);
    if(this.minbuiltup)    this.minbuiltarray.push(this.minbuiltup);
    if(this.maxbuiltup)  this.maxbuiltarray.push(this.maxbuiltup);
    if(this.mincarpet)   this.mincarpetarray.push(this.mincarpet);
    if(this.maxcarpet)  this.maxcarpetarray.push(this.maxcarpet);

    let body =
    {
        "state": this.statearray,
        "city": this.cityarray.length ?  this.cityarray : [],
        "saleType": [],
        "assetType": [],
        "areaUnit": [],
        "assetCategory" : [],
        "furnishing" : this.furnishingstatus,
        "facilities" : [],
        "roomCount":[{
          "roomType":[],
          "roomCount":[]
        }],
        "furniture": [{}] ,
        "homeAppliances":[{}],
        "flats":{
          "floor": [],
          "bhks": this.roomsbhk
        },
        "ceilingAndFlooring": {},
        "bedrooms":[],
        "bathrooms":[],
        "kitchen": [],
        "serventroom":[],
        "poojaroom":[],
        "storeroom":[],
        "studyroom":[],
        "livingroom":[],	
        "diningroom":[],
          "balcony":[],
        "frontyard":[],
        "backyard":[],
        "basement":[],
        "facing": [] ,
        "minCarpetArea":this.mincarpetarray,
        "maxCarpetArea":this.maxcarpetarray,
        "minBuiltUpArea":this.minbuiltarray,
        "maxBuiltUpArea": this.maxbuiltarray,
        "minPrice": this.minPrice,
        "maxPrice": this.maxPrice
  }

  this.apiservice.postFilterProperty(page,body)
  .subscribe((data:any)=>{
   if(data.successCode == "PROPERTY_RETRIVED" && data.statusCode == 200)
    {
      this.isLoading = false;
      this.filterProperty = data.extraData.PROPERTIES;

      this.filterProperty.forEach((data,i) => {
        this.propertyId = data.propertyId

        this.filterProperty[i].furnishing = data.furnishing.replace('_',' ');

        //  const str3 = data.createdAt;
        //   const arr3 = str3.split(/ (.*)/);
        //   this.filterProperty[i].createdAt = new Date(arr3[0]).toDateString();

        this.filterProperty[i].pricing.assetConstructionPhase = this.enumservice.kytname.get(this.filterProperty[i].pricing.assetConstructionPhase);
        this.filterProperty[i].residentialStructuralDocument.propertyFacing = this.enumservice.kytname.get(this.filterProperty[i].residentialStructuralDocument.propertyFacing);
       //  this.recentProperties[i].prebuiltAssetType = this.enumservice.kytname.get(this.recentProperties[i].prebuiltAssetType);

    if(this.filterProperty[i].residentialStructuralDocument.flats)
   {
     this.filterProperty[i].residentialStructuralDocument.flats.bhks = this.enumservice.kytname.get(this.filterProperty[i].residentialStructuralDocument.flats.bhks);

   }

    });
    

      if ($(window).width() < 600) {
        $('.buy-filter').hide();
      }

      console.log("filter",this.filterProperty);
    }
    console.log("filter",data);
    
  })


  }

  ContactSubmit(pid)
  {
    this.spinner = true;
    let body = {
      "name": this.name,
      "mobileNo" : this.mobileNo,
      "email" : this.email,
      "city" : this.city,
      "state":  this.statevalue
    }
    var reg = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/;
  
    console.log(body,this.email);
    console.log("pid",pid);
    
    if(!this.name || !this.mobileNo || !this.email || !this.city || !this.statevalue)
    {
      this.spinner = false;
      this.toastr.info('Please complete Input Field','Incomplete');
    }
    else if(!reg.test(this.email))
    {
      this.spinner = false;
      this.toastr.info('Please Fill Valid Email Address','Invalid Email Address !!');
    }
    else{
    this.apiservice.postConatctForm(pid,body)
    .subscribe((data:any)=>
    {
      this.spinner = false;
      console.log("data",data);
      if(data.successCode == "API_SUCCESS")
      {
        Swal.fire({
          title: "Success",
          text: "Your Form has been Submit Successfully !!",
          type: "info",
          allowEscapeKey: false,
          allowOutsideClick: false,
          }).then(()=>{
            $('#exampleModal').modal('toggle');
          })
      }
      else if(data.exception == 'USER_ALREADY_EXISTS')
      {
        Swal.fire({
          title: "User Info",
          text: "User has been already exists !!",
          type: "info",
          allowEscapeKey: false,
          allowOutsideClick: false,
          }).then(()=>
          {
            $('#exampleModal').modal('toggle');
          })
      }
    },
    (err) => {
      console.log("got error", err)
      //  self.serverDataLogin=err;
    })
  }
  }


  ngOnDestroy()
  {
    console.log("this.recentview before",this.recentview);   
    this.recentview.next();
    console.log("this.recentview after",this.recentview);
    
  }

}
