import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RawpropertydetailComponent } from './rawpropertydetail.component';

describe('RawpropertydetailComponent', () => {
  let component: RawpropertydetailComponent;
  let fixture: ComponentFixture<RawpropertydetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RawpropertydetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RawpropertydetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
