import { Component, OnInit } from '@angular/core';
import Swal from 'sweetalert2';
import * as $ from 'jquery';
import { HttpClient, HttpHeaders } from '@angular/common/http';
declare var $ :any; 
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Router } from '@angular/router';
import { UtilService,ApiService} from '../services';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
@Component({
  selector: 'app-editprofile',
  templateUrl: './editprofile.component.html',
  styleUrls: ['./editprofile.component.scss']
})
export class EditprofileComponent implements OnInit {

  oldpassword="";
  newpassword="";
  confirmPassword="";
  jwttoken:any;
  sessiontoken:any;
  uuid:any;
 
  showbuttonvalue="Show";
  ChangePasswordForm:FormGroup;
  submitted = false;
  step1Form:FormGroup;
  constructor(private formbuilder:FormBuilder,private apiservice:ApiService, private utilservice:UtilService, private router: Router,private http: HttpClient, private toastr: ToastrService) { 
   
    

    this.uuid= localStorage.getItem("uuid");
    this.jwttoken = localStorage.getItem("jwttoken");
    this.sessiontoken = localStorage.getItem("sessiontoken");
    toastr.toastrConfig.preventDuplicates=true;
  }

  ngOnInit() {
    this.ChangePasswordForm = this.formbuilder.group({
       oldpassword:["",Validators.required],
       newpassword:["",Validators.required],
       confirmPassword:["",Validators.required]
    })
    // this.step1Form =this.formbuilder.group({
    //   oldpassword : ['',Validators.required],
    //   newpassword : ['',Validators.required],
    //   confirmPassword : ['',Validators.required],
    // })

  }



  sunmit(){
    console.log('submited',this.step1Form);
 
  }

  myFormdata() {
  
    console.log("==>",this.ChangePasswordForm)
   
  }
  

  LogoutonPortal()
  {
    let body = {
      'uuid': this.uuid,
      "sessionToken": this.sessiontoken
    };
    console.log("this is------>", body)

    var self = this;
    this.apiservice.LogOutPortal(body)
      .subscribe(
        (data) => {
          this.utilservice.Logout(data);
        });
  }


password() 
{
    this.showbuttonvalue=this.showbuttonvalue == 'Show' ? "Hide" : "Show"
}

   
alphabet(e)
{
  var k = e.which;
 var ok = (k >= 65 && k <= 90) || // A-Z(capital letter alpahabets)
     k >= 97 && k <= 122 || // a-z(small letter alpahabets)
     k == 8 ||  // Backspaces
     k == 9 ||  //H Tab
     k == 0 ||  //H Tab
     k == 11 ||  //V Tab
     k == 32 ||  // Space
     k == 127;   //Delete
 if (!ok) {
 // prevent user to press key
     e.preventDefault();
 }
}




validateInpus(){

if(this.confirmPassword != this.newpassword){
    this.toastr.info('Confirm Password not Match','Password Not Match');
    return  false;
  }
  return true;
  }

  ChangePass1()
  {
    console.log('==>');
  }

ChangePass(){
  console.log("==>",this.ChangePasswordForm)
  console.log("===",this.ChangePasswordForm.controls.oldpassword);
  this.submitted = true;

  if(this.ChangePasswordForm.controls.oldpassword.status === 'INVALID')
  {
   this.toastr.info("Please fill input Field","Field is Empty");
  }
  else if(this.ChangePasswordForm.valid && this.validateInpus()){  
    this.submitted = false;     
       
          const httpOptions = {
            headers: new HttpHeaders({
              'Content-Type':  'application/json',
              'Authorization': 'Token ' + this.jwttoken
            })
          }
           let body = {
           'oldpassword': this.oldpassword,
           'newpassword':this.newpassword,
           "uuid" : this.uuid
          };
          console.log("this is------>",body)
         console.log("jwt token is", this.jwttoken);
          // var self=this;
           this.http.post('http://ec2-3-19-70-172.us-east-2.compute.amazonaws.com/api/changePassword', body, httpOptions)
           .subscribe(
           (data) => {
               console.log("successful verify", data);
               this.utilservice.passwordvalidation(data);                 
             }, //For Success Response
                 (err) => {
                 console.log("got error",err)
               } //For Error Response
             );
             
        }
             
      }





}
