import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';
import {RME_URL_MAPPING} from './RME_URL_MAPPING';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import Swal from 'sweetalert2';
@Injectable({
  providedIn: 'root'
})
export class ApiService {
  jwttoken:any;
  httpOptions:any;
  constructor(private http: HttpClient) {
    // this.jwttoken = localStorage.getItem("jwttoken");
    // console.log("=>jwtoken",this.httpOptions,this.jwttoken);

    // this.httpOptions = {
    //   headers: new HttpHeaders({
    //      'Content-Type':  'application/json',
    //      'Authorization': 'Token ' + this.jwttoken})
    //     };
  

   }
   getHttpHeader(){
     let jwt = localStorage.getItem("jwttoken");
     if(jwt){
      let httpOp ={
        headers: new HttpHeaders({
           'Content-Type':  'application/json',
           'Authorization': 'Token ' + jwt})
          };
          return httpOp;
     } else Swal.fire({
      title: "JWT Token Missing",
       text: "Jwt Token is missing!!",
       type: "info",
  })
   }

   postPrebuiltProperty(body){
    return this.http.post(RME_URL_MAPPING.POST_PROPERTY , body , this.getHttpHeader())
   }

   postRawProperty(body){
    return this.http.post(RME_URL_MAPPING.POST_RAW_PROPERTY,body,this.getHttpHeader())
   }

   postFilterProperty(page,body){
    return this.http.post(RME_URL_MAPPING.POST_FILTER + page,body)
   }


   postNeeAanalysis(body){
    return this.http.post(RME_URL_MAPPING.POST_NEED_ANALYSIS,body)
   }

  gettoRecentpproperties(page)
  {
    return this.http.get(RME_URL_MAPPING.GET_Top_RECENT_PROPERTY + page + '&size=20&sort=createdAt,desc');
  }

  getSinglePage(pid)
  {
  return this.http.get(RME_URL_MAPPING.GET_SINGLE_VIEW+pid);
  }


  getleadsdata(pid)
  {
    return this.http.get(RME_URL_MAPPING.GET_LEADS_DATA+pid);
  }

  postConatctForm(pid,body)
  {
    return this.http.post(RME_URL_MAPPING.POST_CONTACT+pid,body)
  }

  getSingleRawPage(pid)
  {
  return this.http.get(RME_URL_MAPPING.GET_RAW_SINGLE_VIEW+pid);
  }

  getOwnerProperties(uuid)
  {
    console.log("=>jwtoken",this.httpOptions);
  return this.http.get(RME_URL_MAPPING.GET_OWNER_DATA + uuid,this.getHttpHeader());
  }

  getuserdata(body) {
    return this.http.post(RME_URL_MAPPING.GET_USER_DATA,body,this.getHttpHeader());
  }

  getTopProperties():Observable<any>
  {
  return this.http.get(RME_URL_MAPPING.GET_TOP_VIEW_Property)
  .pipe(
    map(res => res)
);
  }

  getrawlandproperty()
  {
  return this.http.get(RME_URL_MAPPING.GET_RAW_LAND_PROPERTY);
  }


  getAddViewsproperties(body)
  {
    return this.http.post(RME_URL_MAPPING.GET_ADD_VIEWS,body);
  }

  UploadPropertyImages(uuid,body)
  {
    return this.http.post(RME_URL_MAPPING.POST_POPERTY_IMAGES+uuid,body,this.getHttpHeader());
  }

  LogOutPortal(body)
  {
    return this.http.post(RME_URL_MAPPING.LOG_OUT,body,this.getHttpHeader());
  }




  handleNetworkException(error){
    if(!error.response){
      Swal.fire({
        type: "info",
        title:'Network Error',
        text:'Check the network cables, modem, and router',
        confirmButtonText:'Got It'
      })
    }
  }


}
