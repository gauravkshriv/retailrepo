
export class RME_URL_MAPPING{

    
    public static LOCAL_URL = 'http://localhost:8080';
    public static TESTING_ENV_URL = 'http://ec2-34-234-215-47.compute-1.amazonaws.com';
    public static PRODUCTION_ENV_URL = 'https://apiretail.rmehub.in';

    public static POST_PROPERTY =   RME_URL_MAPPING.PRODUCTION_ENV_URL + '/api/product/prebuilt';

    public static POST_RAW_PROPERTY = RME_URL_MAPPING.PRODUCTION_ENV_URL + '/api/product/raw';
    
    public static POST_NEED_ANALYSIS = RME_URL_MAPPING.PRODUCTION_ENV_URL + '/save/need-analysis';

    public static POST_FILTER = RME_URL_MAPPING.PRODUCTION_ENV_URL + '/filter/retail-products?size=10&pageNo=';
   
    public static GET_Top_RECENT_PROPERTY = RME_URL_MAPPING.PRODUCTION_ENV_URL + '/filter/show/res?page=';
   
    public static GET_TOP_VIEW_Property = RME_URL_MAPPING.PRODUCTION_ENV_URL + '/filter/show/res?page=0&size=10&sort=views,desc';

    public static GET_SINGLE_VIEW = RME_URL_MAPPING.PRODUCTION_ENV_URL + '/product/prebuilt?pid='

    public static GET_LEADS_DATA = RME_URL_MAPPING.PRODUCTION_ENV_URL + 'single-property-lead?pid=';

    public static GET_OWNER_DATA = RME_URL_MAPPING.PRODUCTION_ENV_URL + '/api/user/product/prebuilt?uuid=';

    public static GET_ADD_VIEWS = RME_URL_MAPPING.PRODUCTION_ENV_URL + '/addviewcount';

    public static POST_POPERTY_IMAGES = RME_URL_MAPPING.PRODUCTION_ENV_URL + '/api/uploadattachment?_uat=';

    public static GET_USER_DATA = 'https://api.rmehub.in/api/getuser';

    public static LOG_OUT = 'https://api.rmehub.in/api/logout';

    public static GET_RAW_LAND_PROPERTY =RME_URL_MAPPING.PRODUCTION_ENV_URL + '/filter/show/raw?page=0&size=10&sort=createdAt,desc';

    public static GET_RAW_SINGLE_VIEW =RME_URL_MAPPING.PRODUCTION_ENV_URL + '/product/raw/single?pid=';

    public static POST_CONTACT = RME_URL_MAPPING.PRODUCTION_ENV_URL + '/contact-us?pid=';
}
