import { Injectable } from '@angular/core';
import { RouterStateSnapshot, ActivatedRouteSnapshot, ActivatedRoute } from '@angular/router';
import * as CryptoJS from 'crypto-js';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Router } from '@angular/router';
// import { ResponseService } from '../response.service';

@Injectable({
  providedIn: 'root'
})
export class CheckloginService {
  // public isAuthenticated(): boolean;
  constructor(private http: HttpClient, private router: Router, private activatedRoute: ActivatedRoute) { }

checkLoggedInUser() {
    let uuid = localStorage.getItem("uuid");
    let jwttoken = localStorage.getItem("jwttoken");
    let sessiontoken = localStorage.getItem("sessiontoken");
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + jwttoken
      })
    };
    let body = {
      'uuid': uuid,
      'sessionToken': sessiontoken,
    };
    // console.log("this is------>",body)
     return this.http.post('https://api.rmehub.in/api/user/getSession', body, httpOptions);
  
  }
}